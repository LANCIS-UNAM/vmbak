from sh import virsh, cp
import argparse

parser = argparse.ArgumentParser(description='Backup qemu dom using virsh')
parser.add_argument('--vm', required=True)
parser.add_argument('--bakdir', required=True)
parser.add_argument('--log', required=True)
args = parser.parse_args()



blklist = virsh.domblklist(args.vm)
lines = blklist.split("\n")
blk = lines[2][lines[0].index("Source"):]

overlay = f"{args.bakdir}/{args.vm}_overlay1.qcow2"

virsh("snapshot-create-as",
      "--domain", args.vm, "overlay1",
      "--diskspec", f"vda,file={overlay}",
      no_metadata=True,
      disk_only=True,
      _out=args.log)

cp(blk, args.bakdir,
   verbose=True,
   _out=args.log)

virsh.blockcommit(args.vm, "vda",
                  active=True,
                  verbose=True,
                  pivot=True,
                  _out=args.log)

rm(overlay, verbose=True)
